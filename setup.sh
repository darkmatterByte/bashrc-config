#!/usr/bin/bash
# vars
bashrc=~/.bashrc
cwd=$(dirname $0)
needs_update=0
# create dir and move our config files there
mkdir $bashrc.d 2> /dev/null
cp $cwd/config/* $bashrc.d/

for path in $cwd/config/*
do
	file_path="$bashrc.d/$(basename $path)"
	# dont include more than once
	if [[ -f $bashrc ]] && grep "^source $file_path$" $bashrc &> /dev/null
	then
		echo "$file_path is already being sourced"
	else
		echo "adding $file_path to .bashrc"
		echo "source $file_path" >> ~/.bashrc
		needs_update=1
	fi
done
if [[ $needs_update -gt 0 ]]
then
	echo "all done, now copy this and run in your shell:"
	echo "source $bashrc"
else
	echo "all good"
fi
